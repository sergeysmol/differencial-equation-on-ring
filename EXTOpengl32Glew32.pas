unit EXTOpengl32Glew32;

interface

uses SysUtils, Windows, OpenGL;

type PGLenum = ^GLenum;

const
  GL_VERTEX_ARRAY = 32884;
  GL_NORMAL_ARRAY = 32885;
  GL_COLOR_ARRAY = 32886;
  GL_INDEX_ARRAY = 32887;
  GL_TEXTURE_COORD_ARRAY = 32888;

  GL_RGB4 = 32847;
  GL_RGB5 = 32848;
  GL_RGB8 = 32849;
  GL_RGB10 = 32850;
  GL_RGB12 = 32851;
  GL_RGB16 = 32852;
  GL_RGBA2 = 32853;
  GL_RGBA4 = 32854;
  GL_RGB5_A1 = 32855;
  GL_RGBA8 = 32856;
  GL_RGB10_A2 = 32857;
  GL_RGBA12 = 32858;
  GL_RGBA16 = 32859;
  GL_RGB565 = 36194;

  GL_DEPTH_COMPONENT16 = 33189;
  GL_DEPTH_COMPONENT24 = 33190;
  GL_DEPTH_COMPONENT32 = 33191;

procedure glEnableClientState(cap: GLenum); stdcall;
procedure glDisableClientState(cap: GLenum); stdcall;

procedure glNormalPointer(typed: GLenum; stride: GLsizei; const p: Pointer); stdcall;
procedure glVertexPointer(size: GLint; typed: GLenum; stride: GLsizei; const p: Pointer); stdcall;
procedure glColorPointer(size: GLint; typed: GLenum; stride: GLsizei; const p: Pointer); stdcall;
procedure glTexCoordPointer(size: GLint; typed: GLenum; stride: GLsizei; const p: Pointer); stdcall;

procedure glDrawArrays(mode: GLenum; first: GLint; count: GLsizei); stdcall;
procedure glDrawElements(mode: GLenum; count: GLsizei; typed :GLenum; const indices: Pointer); stdcall;

procedure glGenTextures(n: GLsizei; textures: PGLuint); stdcall;
procedure glDeleteTextures(n: GLsizei; textures: PGLuint); stdcall;
procedure glBindTexture(target:	GLenum; texture: GLuint); stdcall;

implementation

const gl  = 'opengl32.dll';

procedure glEnableClientState(cap: GLenum); stdcall; external gl;
procedure glDisableClientState(cap: GLenum); stdcall; external gl;

procedure glNormalPointer(typed: GLenum; stride: GLsizei; const p: Pointer); stdcall; external gl;
procedure glVertexPointer(size: GLint; typed: GLenum; stride: GLsizei; const p: Pointer); stdcall; external gl;
procedure glColorPointer(size: GLint; typed: GLenum; stride: GLsizei; const p: Pointer); stdcall; external gl;
procedure glTexCoordPointer(size: GLint; typed: GLenum; stride: GLsizei; const p: Pointer); stdcall; external gl;
  
procedure glDrawArrays(mode: GLenum; first: GLint; count: GLsizei); stdcall; external gl;
procedure glDrawElements(mode: GLenum; count: GLsizei; typed :GLenum; const indices: Pointer); stdcall; external gl;

procedure glGenTextures(n: GLsizei; textures: PGLuint); stdcall; external gl;
procedure glDeleteTextures(n: GLsizei; textures: PGLuint); stdcall; external gl;
procedure glBindTexture(target:	GLenum; texture: GLuint); stdcall; external gl;

end.