unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, OpenGL, Unit2, EXTOpengl32Glew32, UnitUxyUDY;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    HelpMenu: TMenuItem;
    AboutMenu: TMenuItem;
    procedure CloseMenuClick(Sender: TObject);
    procedure HelpMenuClick(Sender: TObject);
    procedure AboutMenuClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure do_movement(const Offset: GLfloat); // rotate camera
  private
    { Private declarations }
  public
    // camera rotate angles
    CAlpha: GLfloat;
    CBetta: GLfloat;
    Cdist: GLfloat; // camera ditance
    CMouseClick: GLboolean;
    mdx, mdy: Integer; // mouse offset

    RotM: array[0..15] of GLfloat; // rotate matrix
    ca, cb, sa, sb: GLfloat; // cos & sin rotate angles
    aspect: GLdouble; // windows aspect ration

    StartGridLIndex, OrtsListIndex: GLuint; // OpenGL static lists
    keys: array[0..1023] of GLboolean; // pressed keys
    MyEquationInfo: TDifferencialEquation;
    MyDrawInfo: TEquationDrawInfo;
    UrfList: GLuint; // OpenGL static lists of solution
  end;

const
  MaxRender: GLdouble = 4000;
  MFrec: GLfloat = pi180/4; // mouse speed
  FOV: GLdouble = 90; // Field of View
  ClearColor: array[0..3] of GLfloat = (0.94, 0.94, 0.97, 1.0);
  Cspeed: GLfloat = 0.4; // camera speed
  CdistMin  = 40;  // camera move limit
  CdistMax = 500;
  CPointSize = 2.0;
  CBettaMax: GLfloat = 1.5;
  CBettaMin: GLfloat = -1.5;
  ordW = ord('W'); ordS = ord('S');
  BufferClearBits = GL_DEPTH_BUFFER_BIT or GL_COLOR_BUFFER_BIT;
  
  HelpStr: ShortString ='Rotate camera: clamped left mouse button'+#13#10+
    'Camera move: keys W,S'+#13#10+
    'axis: red Õ, blue Y, green Z';
  AboutStr: ShortString = 'Create by email: sergeysmol4444@yandex.ru'+#13#10+
    'version OpenGL: ';
  RenderTimeStr: ShortString = 'render time (ms) ';

procedure SetDCPixelFormat(const InHDC: HDC);

// boundary conditions of the problem
procedure MyInOutBorder(const x, rxIn, rxOut: PGLdouble);
procedure MyInOutBorderASM(const x, rxIn, rxOut: PGLdouble); // optimized by the number of memory accesses
// rxIn - function value on the inner border of the ring, rxOut -//- outer border.

var
  Form1: TForm1;
  HRC: HGLRC; // OpenGL
  tickCount: byte = 100;
  QPC: TQPC; // Query Performance Timer
  hrdt, invhrf: GLfloat; // render time
  calctimeStr: String;

implementation

{$R *.dfm}

//////////////////////////////////////////////////////////////////
////// boundary conditions  ////////////////////////////////////////

procedure MyInOutBorder(const x, rxIn, rxOut: PGLdouble);
begin
  {$R-}
  rxIn^:=Sqr(x^)*0.1 + sin(3*x^)*0.1;
  rxOut^:=cos(4.5*x^)*sin(2*x^);
  {$R+}
end;

procedure MyInOutBorderASM(const x, rxIn, rxOut: PGLdouble);
const c01: GLfloat = 0.1;
const c45: GLfloat = 4.5;
asm
  {$R-}
  fld qword ptr [x]
  // rxIn
  fld st
  fmul st, st
  fld st(1)
  fadd st, st
  fadd st, st(2)
  fsin
  fadd
  fmul c01
  fstp qword ptr [rxIn]
  // rxOut
  fld st
  fadd st, st
  fsin
  fxch st(1)
  fmul c45
  fcos
  fmulp
  fstp qword ptr [rxOut]
  {$R+}
end;

//////////////////////////////////////////////////////////////////

procedure SetDCPixelFormat(const InHDC: HDC);
var
  pfd: TPixelFormatDescriptor;
  nPixelFormat: Integer;
begin
  {$R-}
  FillChar(pfd, SizeOf(pfd), 0);
  pfd.dwFlags:=PFD_DRAW_TO_WINDOW or PFD_SUPPORT_OPENGL or PFD_DOUBLEBUFFER;

  nPixelFormat:=ChoosePixelFOrmat(InHDC, @pfd);
  SetPixelFormat(InHDC, nPixelFormat, @pfd);
  {$R+}
end;

///////////////////////////////////////////////////////////////////////////////

procedure TForm1.FormCreate(Sender: TObject);
var
  hrf: Int64;
  calctime: GLfloat; // calculate time
begin
  {$R-}
  // inicialize field
  hrdt:=1;
  aspect:=1;

  CAlpha:=-0.7;
  CBetta:=0.5236;
  Cdist:=200;
  CMouseClick:=false;

  ca:=cos(Calpha); cb:=cos(CBetta);
  sa:=sin(Calpha); sb:=sin(CBetta);
  RotM[0]:=ca*cb; RotM[1]:=sa; RotM[2]:=-ca*sb;
  RotM[4]:=-sa*cb; RotM[5]:=ca; RotM[6]:=sa*sb;
  RotM[8]:=sb;  RotM[10]:=cb;
  RotM[3]:=0; RotM[7]:=0; RotM[9]:=0; RotM[11]:=0;
  RotM[12]:=0; RotM[13]:=0; RotM[14]:=0; RotM[15]:=1;

  /////////////////////////////////////
  Form1.KeyPreview:=true;

  mdx:=Self.Width div 2; mdy:=Self.Height div 2;
  SetCursorPos(mdx+Self.Left,mdy+Self.Top);

  QueryPerformanceFrequency(hrf);
  invhrf:=1000/hrf;

  // Solve problem
  MyEquationInfo.Nf:=600;
  MyEquationInfo.Nr:=120;
  MyEquationInfo.N:=MyEquationInfo.Nf*MyEquationInfo.Nr;
  MyEquationInfo.Power:=20;
  MyEquationInfo.RungeQuality:=2048;
  MyEquationInfo.Rmin:=2;
  MyEquationInfo.Rmax:=4;
  MyEquationInfo.InOutBorder:=MyInOutBorderASM;
  MyEquationInfo.PrecissionBorder:=DoublePrecission;
  MyEquationInfo.PrecissionIntegrate:=DoublePrecission;

  QueryPerformanceCounter(QPC.StartPoint);
  SetLength(MyEquationInfo.Urf, MyEquationInfo.N);
  Solve_Urf_Zero(@MyEquationInfo);
  QueryPerformanceCounter(QPC.EndPoint);
  asm
    fild QPC.EndPoint   // st(0) <- Int64
    fild QPC.StartPoint // st(1) <- Int64
    fsub // st(0) = st(0) - st(1)
    fmul invhrf // st(0) = st(0)*invhrf
    fstp calctime   // st(0) -> hrdt
  end;
  calctimeStr:='; Calculate time (ms): ' + FloatToStrF(calctime, ffFixed, 6, 4);

  MyDrawInfo.UXcolorConst:= 0.2; MyDrawInfo.UXcolorLinear:= 0.6;
  MyDrawInfo.UYcolorConst:= 0.5; MyDrawInfo.UYcolorLinear:= 0.0;
  MyDrawInfo.UZcolorConst:= 0.2; MyDrawInfo.UZcolorLinear:= 0.8;
  MyDrawInfo.PointSize:=3.0; MyDrawInfo.PointAlpha:=1.0;
  MyDrawInfo.XScale:=40; MyDrawInfo.ZScale:=60;

  SetDCPixelFormat(Canvas.Handle);
  hrc:=wglCreateContext(Canvas.Handle);
  wglMakeCurrent(Canvas.Handle, hrc);

  InitGL();

  StartGridLIndex:=GenListStartGrid();
  OrtsListIndex:=GenListOrts();
  UrfList:=GetStaticListUrf(MyEquationInfo, MyDrawInfo);

  glViewport(0, 0, ClientWidth, ClientHeight);
  glMatrixMode ( GL_PROJECTION );
  glLoadIdentity;
  aspect:=self.ClientWidth/self.ClientHeight;
  gluPerspective(FOV, aspect, 1, MaxRender);

  glClearColor(ClearColor[0], ClearColor[1], ClearColor[2], ClearColor[3]);
  SetSinglePrecission();
  {$R+}
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  {$R-}
  QueryPerformanceCounter(QPC.StartPoint);

  do_movement(hrdt*CSpeed);

  glMatrixMode ( GL_MODELVIEW );
  glLoadIdentity;
  gluLookAt(
    Cdist, 0, 0, {viewer pos}
    0, 0, 0, {target view}
    0, 0, 1  {up direction}
  );
  glMultMatrixf(@RotM[0]);

  glClear(BufferClearBits);

  glCallList(OrtsListIndex);
  glCallList(StartGridLIndex);
  glCallList(UrfList);

  InvalidateRect(Handle, nil, False );
  SwapBuffers(Canvas.Handle);

  QueryPerformanceCounter(QPC.EndPoint);
  asm
    fild QPC.EndPoint   // st(0) <- Int64
    fild QPC.StartPoint // st(1) <- Int64
    fsub // st(0) = st(0) - st(1)
    fmul invhrf // st(0) = st(0)*invhrf
    fstp hrdt   // st(0) -> hrdt
  end;

  dec(tickCount);
  if tickCount=0 then
    begin
      tickCount:=150;
      Form1.Caption:=RenderTimeStr+FloatToStrF(hrdt, ffFixed, 6, 4)+calctimeStr;
    end;
  {$R+}
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  {$R-}
  glViewport(0, 0, ClientWidth, ClientHeight);
  glMatrixMode ( GL_PROJECTION );
  glLoadIdentity;
  aspect:=self.ClientWidth/self.ClientHeight;
  gluPerspective(FOV, aspect, 1, MaxRender);
  {$R+}
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$R-}
  if (Key<1024) then
    keys[ord(key)]:=true;
  {$R+}
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$R-}
  if (Key<1024) then
    keys[key]:=false;
  {$R+}
end;

procedure TForm1.do_movement(const Offset: GLfloat);
begin
  {$R-}
  if keys[ordW] then Cdist:=Cdist-Offset;
  if keys[ordS] then Cdist:=Cdist+Offset;
  if Cdist<CdistMin then Cdist:=CdistMin;
  if Cdist>CdistMax then Cdist:=CdistMax;
  {$R+}
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  {$R-}
  if (mbLeft=Button) then CMouseClick:=true;
  {$R+}
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  {$R-}
  if (mbLeft=Button) then CMouseClick:=false;
  {$R+}
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  {$R-}
  if CMouseClick then
    begin
      if X<>mdx then
        begin
          CAlpha:=CAlpha + (X-mdX)*MFrec;
          SinCos(@Calpha, @ca, @sa);
          RotM[1]:=sa;
          RotM[5]:=ca;
          RotM[0]:=ca*cb;  RotM[2]:=-ca*sb;
          RotM[4]:=-sa*cb;  RotM[6]:=sa*sb;
        end;
      if Y<>mdY then
        begin
          CBetta:=CBetta + (Y-mdY)*MFrec;
          if CBetta>CBettaMax then CBetta:=CBettaMax; // ÷òî áû íå áûëî ñêà÷êîâ
          if CBetta<CBettaMin then CBetta:=CBettaMin;
          SinCos(@CBetta, @cb, @sb);
          RotM[8]:=sb;
          RotM[10]:=cb;
          RotM[0]:=cb*ca;  RotM[2]:=-sb*ca;
          RotM[4]:=-cb*sa;  RotM[6]:=sb*sa;
        end;
    end;
  mdx:=X; mdy:=Y;
  {$R+}
end;

procedure TForm1.AboutMenuClick(Sender: TObject);
begin
  {$R-}
  ShowMessage(AboutStr+glGetString(GL_VERSION));
  {$R+}
end;

procedure TForm1.HelpMenuClick(Sender: TObject);
begin
  {$R-}
  ShowMessage(HelpStr);
  {$R+}
end;

procedure TForm1.CloseMenuClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  {$R-}
  glDeleteLists(OrtsListIndex, 1);
  glDeleteLists(StartGridLIndex, 1);
  glDeleteLists(UrfList, 1);
  wglDeleteContext(hrc);
  SetLength(MyEquationInfo.Urf, 0);
  SetExtendedPrecission;
  {$R+}
end;

end.