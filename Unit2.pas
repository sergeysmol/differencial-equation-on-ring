unit Unit2;

interface

uses SysUtils, Windows, Classes, Graphics, OpenGL, EXTOpengl32Glew32;

///////////////////////// CONSTANT /////////////////////////
const
	  invpi = 1/pi;
	  pi180 = pi/180;
	  rot2pi = pi + pi;

  startGridVertex: array[0..71] of GLshort = (
    -80,  80,   -60,  80,   -40,  80,   -20,  80, 
      0,  80,    20,  80,    40,  80,    60,  80,
     80,  80,   -80, -80,   -60, -80,   -40, -80,
    -20, -80,     0, -80,    20, -80,    40, -80,
     60, -80,    80, -80,    80, -80,    80, -60,
     80, -40,    80, -20,    80,   0,    80,  20,
     80,  40,    80,  60,    80,  80,   -80, -80,
    -80, -60,   -80, -40,   -80, -20,   -80,   0,
    -80,  20,   -80,  40,   -80,  60,   -80,  80
  );
  startGridIndices: array[0..35] of GLubyte = (
      0,  9,    1, 10,    2, 11,    3, 12,
      4, 13,    5, 14,    6, 15,    7, 16,
      8, 17,   18, 27,   19, 28,   20, 29,
     21, 30,   22, 31,   23, 32,   24, 33,
     25, 34,   26, 35
  );
  
  OrtsVertex: array[0..17] of GLshort = (
    0, 0, 0,    50, 0, 0,
    0, 0, 0,    0, 50, 0,
    0, 0, 0,    0, 0, 50
  );
  OrtsColors: array[0..17] of GLubyte = (
    255, 0, 0,   255, 0, 0,
    0, 0, 255,   0, 0, 255,
    0, 255, 0,   0, 255, 0
  );

type AByte = array of Byte;
type PAByte = ^AByte;
type AGLuint = array of GLuint;
type PAGLuint = ^AGLuint;

type AGLfloat = array of GLfloat;
type AAGLfloat = array of AGLfloat;
type PAGLfloat = ^AGLfloat;
type PAAGLfloat = ^AAGLfloat;

type AGLdouble = array of GLdouble;

type pRGBArray = ^TRGBArray;
  TRGBArray = ARRAY[0..32767] OF TRGBTriple;

type TQPC = record
      StartPoint: Int64;
      EndPoint: Int64;
    end;

type TFPUPrecission = (SinglePrecission = $00, DoublePrecission = $02,
  ExtendedPrecission = $03);

procedure SinCos(Const lpAngle, lpSin, lpCos: PGLfloat);

// FPU float precission
procedure SetExtendedPrecission();
procedure SetDoublePrecission();
procedure SetSinglePrecission();

////////////  OpenGL ////////////////////
function GenListStartGrid(): GLuint;
function GenListOrts(): GLuint;
procedure InitGL();

implementation

procedure SinCos(Const lpAngle, lpSin, lpCos: PGLfloat);
asm
  {$R-}
  fld dword ptr [lpAngle]
  fsincos
  fstp dword ptr [lpSin]
  fstp dword ptr [lpCos]
  {$R+}
end;

procedure SetDoublePrecission();
var
  CWreg: word;
asm
  {$R-}
  push eax
  FSTCW CWreg
  mov ax, CWreg
  and ah, $FC
  or ah, $02
  mov CWreg, ax
  pop eax
  FLDCW CWreg
  {$R+}
end;

procedure SetSinglePrecission();
var
  CWreg: word;
asm
  {$R-}
  push eax
  FSTCW CWreg
  mov ax, CWreg
  and ah, $FC
  mov CWreg, ax
  pop eax
  FLDCW CWreg
  {$R+}
end;

procedure SetExtendedPrecission();
var
  CWreg: word;
asm
  {$R-}
  push eax
  FSTCW CWreg
  mov ax, CWreg
  or ah, $03
  mov CWreg, ax
  pop eax
  FLDCW CWreg
  {$R+}
end;

////// OpenGL äîïîëíåíèå  ////////////////////////

function GenListStartGrid(): GLuint;
const StartGridColor: array[0..2] of GLubyte = (76, 76, 127);
begin
  {$R-}
  result:=glGenLists(1);
  if result=0 then Exit;

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_SHORT, 0, @startGridVertex);

  glNewList(result, GL_COMPILE); // compile mode
  glLineWidth(1);
  glColor3ubv(@StartGridColor);
  glDrawElements(GL_LINES, 36, GL_UNSIGNED_BYTE, @startGridIndices);
	glEndList();	//=========================================================

	glDisableClientState(GL_VERTEX_ARRAY);	// disable vertex arrays
  {$R+}
end;

function GenListOrts(): GLuint;
begin
  {$R-}
  result:=glGenLists(1);
  if result=0 then Exit;

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  
  glVertexPointer(3, GL_SHORT, 0, @OrtsVertex);
  glColorPointer(3, GL_UNSIGNED_BYTE, 0, @OrtsColors);

  glNewList(result, GL_COMPILE); // compile mode
  glLineWidth(1.5);
  glDrawArrays(GL_LINES, 0, 6);
	glEndList();	//=========================================================

  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
  {$R+}
end;

procedure InitGL();
begin
  {$R-}
  glEnable(GL_TEXTURE_2D); 
  glEnable(GL_DEPTH_TEST);
  glClear(GL_DEPTH_BUFFER_BIT);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glPolygonMode(GL_FRONT, GL_FILL);

  glDepthMask ( GL_TRUE );
  glDepthFunc(GL_LEQUAL);

  glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);

  glShadeModel(GL_POLYGON_SMOOTH);

  glEnable(GL_ALPHA_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glAlphaFunc(GL_LEQUAL,1);


  glEnable(GL_POINT_SMOOTH);
  glHint(GL_POINT_SMOOTH_HINT, GL_NICEST); //GL_Fastest, GL_NICEST

  glEnable(GL_LINE_SMOOTH);
  glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

  glEnable(GL_POLYGON_SMOOTH);
  glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

  glHint(GL_Perspective_Correction_Hint, GL_NICEST);
  {$R+}
end;

end.
