unit UnitUxyUDY;

interface

uses SysUtils, Windows, Unit2, OpenGL, EXTOpengl32Glew32;

type TBorderProc = procedure(const Arg, InBorder, OutBorder: PGLdouble);

type TRungeInOutBorder = record
        InData, InData2, InData4: AGLdouble;
        OutData, OutData2, OutData4: AGLdouble;
        tData, tData2, tData4: AGLdouble;
        k, k2, k4: GLuint;
        dtk, dtk2, dtk4: GLdouble;
        Precission: TFPUPrecission;
      end;
    PRungeInOutBorder = ^TRungeInOutBorder;

type TProgonka = record
        SolutionData, AK, AL, invX: AGLfloat;
        SolutionSize: GLuint;
        Xmin, Xmax, dx: GLfloat;
        f3, f6: GLfloat;
        Power: GLuint;
      end;
    PProgonka = ^TProgonka;

type TDifferencialEquation = record
        Urf: AGLfloat;
        Nf, Nr, N, Power, RungeQuality: GLuint;
        Rmin, Rmax, Umin, Umax: GLfloat;
        InOutBorder: TBorderProc;
        PrecissionBorder: TFPUPrecission;
        PrecissionIntegrate: TFPUPrecission;
      end;
    PDifferencialEquation = ^TDifferencialEquation;

type TEquationDrawInfo = record
        UXcolorConst, UXcolorLinear: GLfloat;
        UYcolorConst, UYcolorLinear: GLfloat;
        UZcolorConst, UZcolorLinear: GLfloat;
        PointSize, PointAlpha: GLfloat;
        XScale, ZScale: GLfloat;
      end;
    PEquationDrawInfo = ^TEquationDrawInfo;

procedure PreRungeInOutBorder(const RungeInOutBorder: PRungeInOutBorder;
  const Power: GLuint);
procedure GetRungeInOutBorder(const RungeInOutBorder: PRungeInOutBorder;
  const InOutBorder: TBorderProc);
// initialization for GetF3F6_Runge

procedure FreeRungeInOutBorder(const RungeInOutBorder: PRungeInOutBorder);

procedure GetF3F6_RungeCos(const Progonka: PProgonka;
  const RungeInOutBorder: TRungeInOutBorder);
procedure GetF3F6_RungeSin(const Progonka: PProgonka;
  const RungeInOutBorder: TRungeInOutBorder);
// this function considers the integral of the functions InBorder\OutBorder of one argument
// on interval [-pi,pi] for the coefficient of decomposition of the Fourier series
// with correction Runge-Kutta by 3 steps
// Cos - coefficient at cos Fourier series
// Sin - coefficient at sin Fourier series

procedure PreProgonka(const Progonka: PProgonka);
// get dx, get memory for SolutionData, AK, AL, invX
// initialization invX
procedure FreeProgonka(const Progonka: PProgonka);
// clear SolutionData, AK, AL, invX

procedure GetProgonka(const Progonka: PProgonka);
// This procedure solves an ordinary second order differential equation of the form:
// U'' - p(x)*U' + q(x,k)*U = r(x); with boundary conditions:
// f1*U(x_min ) + f2*U'(x_min) = f3; f4*U(x_max ) + f5*U'(x_max) = f6
// by sweep method.

procedure Solve_Urf_Zero(const EquationInfo: PDifferencialEquation);

function GetStaticListUrf(const EquationInfo: TDifferencialEquation;
 const DrawInfo: TEquationDrawInfo): GLuint;

implementation

///////////////// ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

procedure PreRungeInOutBorder(const RungeInOutBorder: PRungeInOutBorder;
  const Power: GLuint);
var
  i: GLuint;
begin
  {$R-}
  with RungeInOutBorder^ do
    begin
      k:=Power;
      k2:=Power shl 1;
      k4:=Power shl 2;

      SetLength(InData, k+1);
      SetLength(InData2, k2);
      SetLength(InData4, k4);
      SetLength(OutData, k+1);
      SetLength(OutData2, k2);
      SetLength(OutData4, k4);
      SetLength(tData, k);
      SetLength(tData2, k2);
      SetLength(tData4, k4);

      dtk:=rot2pi/k;
      dtk2:=rot2pi/k2;
      dtk4:=rot2pi/k4;

      case RungeInOutBorder.Precission of
        SinglePrecission: SetSinglePrecission;
        DoublePrecission: SetDoublePrecission;
        ExtendedPrecission: SetExtendedPrecission;
      end;
      for i:=k-1 downto 0 do
        tData[i]:=i*dtk - Pi;
      for i:=k2-1 downto 0 do
        tData2[i]:=i*dtk2 - Pi;
      for i:=k4-1 downto 0 do
        tData4[i]:=i*dtk4 - Pi;
      SetExtendedPrecission;
    end;
  {$R+}
end;

procedure GetRungeInOutBorder(const RungeInOutBorder: PRungeInOutBorder;
  const InOutBorder: TBorderProc);
var
  i: GLuint;
begin
  {$R-}
  with RungeInOutBorder^ do
    begin
      InData[k]:=Pi;
      OutData[k]:=Pi;
      case RungeInOutBorder.Precission of
        SinglePrecission: SetSinglePrecission;
        DoublePrecission: SetDoublePrecission;
        ExtendedPrecission: SetExtendedPrecission;
      end;
      for i:=k-1 downto 0 do
        InOutBorder(@tData[i], @InData[i], @OutData[i]);
      for i:=k2-1 downto 0 do
        InOutBorder(@tData2[i], @InData2[i], @OutData2[i]);
      for i:=k4-1 downto 0 do
        InOutBorder(@tData4[i], @InData4[i], @OutData4[i]);
      SetExtendedPrecission();
    end;
  {$R+}
end;

procedure FreeRungeInOutBorder(const RungeInOutBorder: PRungeInOutBorder);
begin
  {$R-}
  with RungeInOutBorder^ do
    begin
      SetLength(InData, 0);
      SetLength(InData2, 0);
      SetLength(InData4, 0);
      SetLength(OutData, 0);
      SetLength(OutData2, 0);
      SetLength(OutData4, 0);
      SetLength(tData, 0);
      SetLength(tData2, 0);
      SetLength(tData4, 0);
    end;
  {$R+}
end;

procedure PreProgonka(const Progonka: PProgonka);
var
  i: GLuint;
begin
  {$R-}
  Progonka.dx:=(Progonka.xmax-Progonka.xmin)/Progonka.SolutionSize;
  with Progonka^ do
    begin
      SetLength(AK, SolutionSize);
      SetLength(AL, SolutionSize);
      SetLength(invX, SolutionSize);
      Progonka.AL[0]:=0;
      for i:=SolutionSize-1 downto 0 do
        invX[i]:=1/(xmin + i*Progonka.dx);
    end;
  {$R+}
end;

procedure GetF3F6_RungeCos(const Progonka: PProgonka;
  const RungeInOutBorder: TRungeInOutBorder);
var
  i: GLuint;
  cosPt: Extended;
  R0_1, R1_1, R2_1, kp_1, Fhsum_1: Extended;
  R0_2, R1_2, R2_2, kp_2, Fhsum_2: Extended;
begin
  {$R-}
  with Progonka^, RungeInOutBorder do
    begin
      cosPt:=cos(Power*Pi)*0.5;
      Fhsum_1:=cosPt*(InData[k] - InData[0]);
      Fhsum_2:=cosPt*(OutData[k] - OutData[0]);

      R0_1:=0;
      R0_2:=0;
      //dtk
      for i:=k-1 downto 0 do
        begin
          cosPt:=cos(Power*tData[i]);
          R0_1:=R0_1 + cosPt*InData[i];
          R0_2:=R0_2 + cosPt*OutData[i];
        end;
      R0_1:=R0_1+Fhsum_1;
      R0_2:=R0_2+Fhsum_2;

      R1_1:=0;
      R1_2:=0;
      //dtk2
      for i:=k2-1 downto 0 do
        begin
          cosPt:=cos(Power*tData2[i]);
          R1_1:=R1_1 + cosPt*InData2[i];
          R1_2:=R1_2 + cosPt*OutData2[i];
        end;
      R1_1:=(R1_1+Fhsum_1)*dtk2;
      R1_2:=(R1_2+Fhsum_2)*dtk2;


      //dtk4
      R2_1:=0;
      R2_2:=0;
      for i:=k4-1 downto 0 do
        begin
          cosPt:=cos(Power*tData4[i]);
          R2_1:=R2_1 + cosPt*InData4[i];
          R2_2:=R2_2 + cosPt*OutData4[i];
        end;
      R2_1:=(R2_1+Fhsum_1)*dtk4;
      R2_2:=(R2_2+Fhsum_2)*dtk4; 

      if R0_1=R1_1 then
        begin
          f3:=R1_1*invPi;
        end
      else
        begin
          f3:=R0_1 - R1_1;
          kp_1:=(R1_1 - R2_1)/f3;
          R1_1:=f3/(kp_1 - 1);
          f3:=(R0_1 + R1_1)*invPi;
        end; 

      if R0_2=R1_2 then
        begin
          f6:=R1_2*invPi;
        end
      else
        begin
          f6:=R0_2 - R1_2;
          kp_2:=(R1_2 - R2_2)/f6;
          R1_2:=f6/(kp_2 - 1);
          f6:=(R0_2 + R1_2)*invPi;
        end;
    end;
  {$R+}
end;

procedure GetF3F6_RungeSin(const Progonka: PProgonka;
  const RungeInOutBorder: TRungeInOutBorder);
var
  i: GLuint;
  sinPt: Extended;
  R0_1, R1_1, R2_1, kp_1: Extended;
  R0_2, R1_2, R2_2, kp_2: Extended;
begin
  {$R-}
  with Progonka^, RungeInOutBorder do
    begin
      R0_1:=0;
      R0_2:=0;
      //dtk
      for i:=k-1 downto 0 do
        begin
          sinPt:=Sin(Power*tData[i]);
          R0_1:=R0_1 + sinPt*InData[i];
          R0_2:=R0_2 + sinPt*OutData[i];
        end;

      R1_1:=0;
      R1_2:=0;
      //dtk2
      for i:=k2-1 downto 0 do
        begin
          sinPt:=Sin(Power*tData2[i]);
          R1_1:=R1_1 + sinPt*InData2[i];
          R1_2:=R1_2 + sinPt*OutData2[i];
        end;
      R1_1:=R1_1*dtk2;
      R1_2:=R1_2*dtk2;

      //dtk4
      R2_1:=0;
      R2_2:=0;
      for i:=k4-1 downto 0 do
        begin
          sinPt:=sin(Power*tData4[i]);
          R2_1:=R2_1 + sinPt*InData4[i];
          R2_2:=R2_2 + sinPt*OutData4[i];
        end;
      R2_1:=R2_1*dtk4;
      R2_2:=R2_2*dtk4;

      if R0_1=R1_1 then
        begin
          f3:=R1_1*invPi;
        end
      else
        begin
          f3:=R0_1 - R1_1;
          kp_1:=(R1_1 - R2_1)/f3;
          R1_1:=f3/(kp_1 - 1);
          f3:=(R0_1 + R1_1)*invPi;
        end; 

      if R0_2=R1_2 then
        begin
          f6:=R1_2*invPi;
        end
      else
        begin
          f6:=R0_2 - R1_2;
          kp_2:=(R1_2 - R2_2)/f6;
          R1_2:=f6/(kp_2 - 1);
          f6:=(R0_2 + R1_2)*invPi;
        end;
    end;
  {$R+}
end;

procedure FreeProgonka(const Progonka: PProgonka);
begin
  {$R-}
  with Progonka^ do
    begin
      SetLength(AK, 0);
      SetLength(AL, 0);
      SetLength(invX, 0);
    end;
  {$R+}
end;

procedure GetProgonka(const Progonka: PProgonka);
var
  i: GLuint;
  invxdx, znam, tmp: GLfloat;
begin
  {$R-}
  with Progonka^ do
    begin
      tmp:=(Sqr(Power) shl 1)*Sqr(dx);
      AK[0]:=f3;
      SolutionData[0]:=f3;
      SolutionData[SolutionSize-1]:=f6;

      znam:=-1/(tmp*sqr(invx[1]) + 4);
      invxdx:=invx[1]*dx;
      AK[1]:=(invxdx - 2)*f3*znam;
      AL[1]:=(2 + invxdx)*znam;
      For i:=2 to SolutionSize-1 do
        begin
          znam:=tmp*sqr(invx[i]) + 4;
          invxdx:=dx*invx[i] - 2;
          znam:=1/(AL[i-1]*invxdx - znam);
          AK[i]:=znam*invxdx*AK[i-1];
          AL[i]:=(4 + invxdx)*znam;
        end;

      For i:=SolutionSize-2 downto 1 do
        SolutionData[i]:=AK[i] - AL[i]*SolutionData[i+1];
    end;
  {$R+}
end;

procedure Solve_Urf_Zero(const EquationInfo: PDifferencialEquation);
var
  i, j, k, offset: GLuint;
  Progonka: TProgonka;
  RungeInOutBorder: TRungeInOutBorder;
  Fpt: AGLfloat;
  Ya, Yb: AAGLfloat;
  df, f, cosf, sinf, U: GLfloat;
begin
  {$R-}
  RungeInOutBorder.Precission:=EquationInfo.PrecissionBorder;
  PreRungeInOutBorder(@RungeInOutBorder, EquationInfo.RungeQuality);
  GetRungeInOutBorder(@RungeInOutBorder, EquationInfo.InOutBorder);

  Progonka.SolutionSize:=EquationInfo.Nr;
  Progonka.Xmin:=EquationInfo.Rmin;
  Progonka.Xmax:=EquationInfo.Rmax;
  PreProgonka(@Progonka);

  SetLength(Ya, EquationInfo.Power+1, EquationInfo.Nr);
  SetLength(Yb, EquationInfo.Power+1, EquationInfo.Nr);
  case RungeInOutBorder.Precission of
        SinglePrecission: SetSinglePrecission;
        DoublePrecission: SetDoublePrecission;
        ExtendedPrecission: SetExtendedPrecission;
      end;
  for k:=EquationInfo.Power downto 0 do
    begin
      Progonka.Power:=k;
      Progonka.SolutionData:=Ya[k];
      GetF3F6_RungeCos(@Progonka, RungeInOutBorder);
      GetProgonka(@Progonka);
      Progonka.SolutionData:=Yb[k];
      GetF3F6_RungeSin(@Progonka, RungeInOutBorder);
      GetProgonka(@Progonka);
    end;

  FreeProgonka(@Progonka);
  FreeRungeInOutBorder(@RungeInOutBorder);

  SetLength(Fpt, EquationInfo.Nf);
  df:=rot2pi/EquationInfo.Nf;
  for i:=EquationInfo.Nf-1 downto 0 do Fpt[i]:=i*df - pi;

  // we collect the solution from the obtained coefficients of the Fourier expansion
  for i:=EquationInfo.Nf-1 downto 0 do
    begin
      offset:=i*EquationInfo.Nr;
      for j:=EquationInfo.Nr-1 downto 0 do EquationInfo.Urf[offset+j]:=0;
      for k:=EquationInfo.Power downto 0 do
        begin
          f:=k*Fpt[i];
          cosf:=cos(f);
          sinf:=sin(f);
          for j:=EquationInfo.Nr-1 downto 0 do
            EquationInfo.Urf[offset+j]:=EquationInfo.Urf[offset+j]
              + Ya[k][j]*cosf + Yb[k][j]*sinf;
        end;
    end;

  // find min and max of solution
  EquationInfo.Umin:=EquationInfo.Urf[EquationInfo.N-1];
  EquationInfo.Umax:=EquationInfo.Umin;
  for i:=EquationInfo.Nf-2 downto 0 do
    begin
      offset:=i*EquationInfo.Nr;
      U:=EquationInfo.Urf[offset];
      if EquationInfo.Umin>U then EquationInfo.Umin:=U;
      if EquationInfo.Umax<U then EquationInfo.Umax:=U;
      offset:=offset + EquationInfo.Nr-1;
      U:=EquationInfo.Urf[offset];
      if EquationInfo.Umin>U then EquationInfo.Umin:=U;
      if EquationInfo.Umax<U then EquationInfo.Umax:=U;
    end;
  SetExtendedPrecission();

  SetLength(Fpt, 0);
  SetLength(Ya, 0, 0);
  SetLength(Yb, 0, 0);
  {$R+}
end;

function GetStaticListUrf(const EquationInfo: TDifferencialEquation;
 const DrawInfo: TEquationDrawInfo): GLuint;
var
  i, j, offset: GLuint;
  Upt, Ucl, Rcl: AGLFloat;
  df, dr, YColor, RInterval, invUInterval, U: GLfloat;
begin
  {$R-}
  SetSinglePrecission();
  result:=glGenLists(1);
  if result=0 then Exit;
  
  df:=360/EquationInfo.Nf;
  dr:=1/EquationInfo.Nr;
  SetLength(Rcl, EquationInfo.Nr);
  for j:=EquationInfo.Nr-1 downto 0 do Rcl[j]:=j*dr;
  RInterval:=(EquationInfo.Rmax - EquationInfo.Rmin);

  invUInterval:=1/(EquationInfo.Umax - EquationInfo.Umin);

  SetLength(Upt, EquationInfo.Nr*3);
  Setlength(Ucl, EquationInfo.Nf*4);

  for j:=EquationInfo.Nr-1 downto 0 do
    begin
      Ucl[4*j]:=DrawInfo.UXcolorConst + DrawInfo.UXcolorLinear*Rcl[j];
      Ucl[4*j + 3]:=DrawInfo.PointAlpha;

      Upt[3*j]:=EquationInfo.Rmin + Rcl[j]*RInterval;
      Upt[3*j + 1]:=0;
    end;

  glNewList(result, GL_COMPILE); // compile mode
  glPointSize(DrawInfo.PointSize);
  glScalef(DrawInfo.XScale, DrawInfo.XScale, DrawInfo.ZScale);
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, Upt);
  glColorPointer(4, GL_FLOAT, 0, Ucl);

  for i:=EquationInfo.Nf-1 downto 0 do
    begin
      YColor:=DrawInfo.UYcolorConst + DrawInfo.UYcolorLinear*i/EquationInfo.Nf;
      offset:=i*EquationInfo.Nr;
      for j:=EquationInfo.Nr-1 downto 0 do
        begin
          U:=EquationInfo.Urf[offset+j];
          Ucl[4*j + 1]:=YColor;
          Ucl[4*j + 2]:=DrawInfo.UZcolorConst + DrawInfo.UZcolorLinear*(
            U - EquationInfo.Umin)*invUInterval;

          Upt[3*j + 2]:=U;
        end;
      glDrawArrays(GL_POINTS, 0, EquationInfo.Nr);
      glRotatef(df, 0.0, 0.0, 1.0);  
    end;
  glDisableClientState( GL_VERTEX_ARRAY );
  glDisableClientState(GL_COLOR_ARRAY);
  glEndList();	//=======

  SetExtendedPrecission();

  SetLength(Upt, 0);
  Setlength(Ucl, 0);
  SetLength(Rcl, 0);
  {$R+}
end;

end.
